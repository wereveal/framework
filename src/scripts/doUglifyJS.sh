#!/usr/bin/env bash
if [ -d public/assets/js/ ]; then
  thePublicDir='public/assets/js/'
elif [ -d ../../public/assets/js/ ]; then
  thePublicDir='../../public/assets/js/'
else
  exit 1
fi

if [ -d src/namespaces/ ]; then
 namespacesDir='src/namespaces'
elif [ -d ../namespaces/ ]; then
 namespacesDir='../namespaces'
fi

for dir in "$namespacesDir"/*
do
  for inner_dir in "$namespacesDir"/"$dir"/*
  do
    theJsDir=$namespacesDir/$dir/$inner_dir/resources/assets/js
    if [ -d "$theJsDir" ]; then
      for thing in "$theJsDir"/*.js
      do
        shortThing=$(basename "$thing")
        uglifyjs --compress --mangle --source-map --output "$thePublicDir"/"$shortThing" "$thing"
      done
    fi
  done
done
