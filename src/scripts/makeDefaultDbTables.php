<?php
/**
 * @brief     This file sets up the database.
 * @details   This creates the database tables and inserts default data.
 *            This should be run from the cli in the /src/bin directory of the site.
 *            Copy /src/config/install_files/install_config.php.txt to /src/config/install_config.php.
 *            The copied file may have any name as long as it is in /src/config directory, but then it needs to be
 *            called on the cli, e.g. php makeDb.php my_install_config.php
 * @file      /src/bin/makeDb.php
 * @namespace Ritc
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @date      2017-12-15 15:52:40
 * @version   3.1.0
 * @TODO      Actually get this script to work.
 * @note   <b>Change Log</b>
 * - v3.1.0 - Updated for name change to NewNsGroupHelper           - 2023-05-02 wer
 * - v3.0.0 - Changed to use DbCreator and NewAppHelper             - 2017-12-15 wer
 * - v2.5.0 - Added several files to be created in app.             - 2017-05-25 wer
 * - v2.4.0 - changed several settings, defaults, and actions       - 2017-05-11 wer
 * - v2.3.0 - fix to install_files setup.php in public dir          - 2017-05-08 wer
 * - v2.2.0 - bug fixes to get postgresql working                   - 2017-04-18 wer
 * - v2.1.0 - lots of bug fixes and additions                       - 2017-01-24 wer
 * - v2.0.0 - bug fixes and rewrite of the database insert stuff    - 2017-01-13 wer
 * - v1.0.0 - initial version                                       - 2015-11-27 wer
 */
namespace Ritc;

use PDO;
use Ritc\Library\Exceptions\FactoryException;
use Ritc\Library\Exceptions\ModelException;
use Ritc\Library\Exceptions\ServiceException;
use Ritc\Library\Factories\PdoFactory;
use Ritc\Library\Helper\AutoloadMapper;
use Ritc\Library\Models\DbCreator;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Services\Di;
use Ritc\Library\Services\Elog;

if (!str_contains(__DIR__, '/src/scripts')) {
    die('Please Run this script from the src/scripts directory');
}
$base_path = str_replace('/src/scripts', '', __DIR__);
define('DEVELOPER_MODE', true);
define('BASE_PATH', $base_path);
define('PUBLIC_PATH', $base_path . '/public');

require_once BASE_PATH . '/src/config/constants.php';

if (!file_exists(NAMESPACES_PATH . '/Ritc/Library')) {
    die("You must clone the Ritc/Library in the namespaces dir first and any other desired namespaces.\n");
}

$install_files_path = SRC_CONFIG_PATH . '/install_files';

/* allows a custom file to be created. Still must be in src/config dir */
$install_config = SRC_CONFIG_PATH . '/install_config.php';
if (isset($argv[1])) {
    $install_config = SRC_CONFIG_PATH . '/' . $argv[1];
}
if (!file_exists($install_config)) {
    die('You must create the install_configs configuration file in ' . SRC_CONFIG_PATH . "The default name for the file is install_config.php. You may name it anything but it must then be specified on the command line.\n");
}
$a_install = require $install_config;
$a_required_keys = [
    'nsgroup_name',
    'namespace',
    'db_file',
    'db_host',
    'db_type',
    'db_port',
    'db_name',
    'db_user',
    'db_pass',
    'db_persist',
    'db_errmode',
    'db_prefix',
    'lib_db_prefix'
];
foreach ($a_required_keys as $key) {
    if (empty($a_install[$key])) {
        die('The install config file does not have required values');
    }
}
$a_needed_keys = [
    'author',
    'short_author',
    'email',
    'loader',
    'superadmin',
    'admin',
    'manager',
    'developer_mode',
    'public_path',
    'base_path',
    'server_http_host',
    'domain',
    'tld',
    'specific_host'
];
foreach ($a_needed_keys as $key) {
    if (!isset($a_install[$key])) {
        $a_install[$key] = '';
    }
}

### generate files for autoloader ###
require NAMESPACES_PATH . '/Ritc/Library/Helper/AutoloadMapper.php';
$a_dirs = [
    'src_path'        => SRC_PATH,
    'config_path'     => SRC_CONFIG_PATH,
    'namespaces_path' => NAMESPACES_PATH
];
$o_cm = new AutoloadMapper($a_dirs);
if (!is_object($o_cm)) {
    die('Could not instance AutoloadMapper');
}
$o_cm->generateMapFiles();
$nsgroup_path = NAMESPACES_PATH . '/' . $a_install['namespace'] . '/' . $a_install['nsgroup_name'];
### Setup the database ###
$db_config_file = $a_install['db_file'];
$db_config_file_text =<<<EOT
<?php
return [
    'driver'     => '{$a_install['db_type']}',
    'host'       => '{$a_install['db_host']}',
    'port'       => '{$a_install['db_port']}',
    'name'       => '{$a_install['db_name']}',
    'user'       => '{$a_install['db_user']}',
    'password'   => '{$a_install['db_pass']}',
    'userro'     => '{$a_install['db_user']}',
    'passro'     => '{$a_install['db_pass']}',
    'persist'    => {$a_install['db_persist']},
    'prefix'     => '{$a_install['db_prefix']}',
    'errmode'    => '{$a_install['db_errmode']}',
    'db_prefix'  => '{$a_install['db_prefix']}',
    'lib_prefix' => '{$a_install['lib_db_prefix']}'
];
EOT;

file_put_contents(SRC_CONFIG_PATH . '/' . $db_config_file, $db_config_file_text);

$o_loader = require VENDOR_PATH . '/autoload.php';

if ($a_install['loader'] === 'psr0') {
    $my_classmap = require SRC_CONFIG_PATH . '/autoload_classmap.php';
    $o_loader->addClassMap($my_classmap);
}
else {
    $my_namespaces = require SRC_CONFIG_PATH . '/autoload_namespaces.php';
    foreach ($my_namespaces as $psr4_prefix => $psr0_paths) {
        $o_loader->addPsr4($psr4_prefix, $psr0_paths);
    }
}

$o_di = new Di();
try {
    $o_pdo = PdoFactory::start($db_config_file, 'rw');
}
catch (FactoryException $e) {
    die('Unable to start the PdoFactory. ' . $e->errorMessage());
}

if ($o_pdo !== false) {
    $o_db = new DbModel($o_pdo, $db_config_file);
    if (!$o_db instanceof DbModel) {
        die("Could not get the database to work\n");
    }

    $o_di->set('db', $o_db);
}
else {
    die("Could not connect to the database\n");
}

switch ($a_install['db_type']) {
    case 'pgsql':
        $a_sql = require $install_files_path .  '/default_pgsql_create.php';
        break;
    case 'sqlite':
        $a_sql = array();
        break;
    case 'mysql':
    default:
        $a_sql = require $install_files_path .  '/default_mysql_create.php';
}
$a_data = require $install_files_path .  '/default_data.php';

$o_di->setVar('a_sql', $a_sql);
$o_di->setVar('a_data', $a_data);
$o_di->setVar('a_install_config', $a_install);
$o_di->setVar('nsgroup_path', $nsgroup_path);

/**
 * Creates the strings needed for sql.
 * @param array $a_records
 * @return array
 */
function createStrings(array $a_records = []) {
    $a_record = array_shift($a_records);
    $fields = '';
    $values = '';
    foreach ($a_record as $key => $a_value) {
        $fields .= $fields === '' ? $key : ', ' . $key;
        $values .= $values === '' ? ':' . $key : ', :' . $key;
    }
    return [
        'fields' => $fields,
        'values' => $values
    ];
}

/**
 * Reorganizes the array.
 * @param array $a_org_values
 * @return array
 */
function reorgArray(array $a_org_values = []) {
    $a_values = [];
    foreach ($a_org_values as $a_value) {
        $a_values[] = $a_value;
    }
    return $a_values;
}

/**
 * Rolls back the transaction and exits the script.
 *
 * @param DbModel $o_db
 * @param string  $message
 * @param bool    $rollback
 */
function failIt(DbModel $o_db, $message = '', $rollback = true) {
    if ($rollback) {
        try {
            $o_db->rollbackTransaction();
        }
        catch (ModelException $e) {
            print 'Could not rollback transaction: ' . $e->errorMessage() . "\n";
        }
    }
    die("\n{$message}\n");
}

try {
    $o_db->startTransaction();
}
catch (ModelException $e) {
    print 'Could not start transaction: ' . $e->errorMessage() . "\n";
}
$o_installer_model = new DbCreator($o_di);
print 'Creating Databases: ';
if (!$o_installer_model->createTables()) {
    failIt($o_db, $o_installer_model->getErrorMessage());
}
print "success\n";
try {
    $o_db->commitTransaction();
}
catch (ModelException $e) {
    failIt($o_db, $e->errorMessage());
}
