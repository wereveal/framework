#!/bin/bash
useBulma="n"
useBootstrap="y"
useDefault="n"
useExpanded="compressed"
updateOnly="--update "
rootDir=$(pwd)
while getopts ":b:d:e:f:g:n" opt; 
do
    case $opt in
        b)
            useBulma="y"
            useBootstrap="n"
            ;;
        d)
          useBulma="n"
          useBootstrap="y"
          ;;
        e)
          useExpanded="expanded"
          ;;
        f)
          updateOnly=""
          ;;
        g)
          useDefault="y"
          ;;
        n)
          useBulma="n"
          useBootstrap="n"
          ;;
        \?)
            echo "Valid options are 
                  -d Bootstrap (default), 
                  -b for Bulma (default use bootstrap),
                  -e compile expanded format (default compressed),
                  -g compile the generic scss file (default don't),
                  -f force compile of everything (default update only)
                  -n don't use either framework (default use bootstrap)
                  " >&2
            ;;
    esac
done
if [ -d "$rootDir"/public/assets/css ]; 
then
  thePublicDir=$rootDir'/public/assets/css/'
  nmDir='node_modules'
elif [ -d ../public/assets/css ]; 
then
  thePublicDir='../public/assets/css'
  nmDir='../node_modules'
elif [ -d ../../public/assets/css ]; 
then
  thePublicDir='../../public/assets/css/'
  nmDir='../../node_modules'
else
  exit 1
fi

# compile the default styles sheet
if [ "$useDefault" = "y" ]
then
  if [ -d "$rootDir"/src/scss/ ]; 
  then
    theDir=$rootDir'/src/scss/'
  elif [ -d ../scss/ ];
  then
    theDir='../scss/'
  else
    exit 1
  fi

  if [ "$useBulma" = "y" ];
  then
    sass --load-path="$nmDir"/bulma "$updateOnly"--style="$useExpanded" ${theDir}:${thePublicDir}
  fi
  if [ "$useBootstrap" = "y" ];
  then
    sass --load-path="$nmDir"/bootstrap "$updateOnly"--style="$useExpanded" ${theDir}:${thePublicDir}
  fi
  if [ "$useBootstrap" = "n" ] && [ "$useBootstrap" = "n" ];
  then
    sass "$updateOnly"--style="$useExpanded" ${theDir}:${thePublicDir}
  fi
fi

if [ -d "$rootDir"/src/namespaces/ ];
then
 namespacesDir=$rootDir"/src/namespaces";
elif [ -d ../namespaces/ ];
then
 namespacesDir="../namespaces";
fi

for theNamespace in "$namespacesDir"/*
do
  for theSection in "$theNamespace"/*
  do
    if [ -d "$theSection" ];
    then
      theScssDir="$theSection"/resources/assets/scss/

      if [ -d "$theScssDir" ];
      then
        baseSassCmd="$updatedOnly--style=$useExpanded $theScssDir:$thePublicDir"

        if [ "$useBulma" = "y" ];
        then
          sass --load-path="$nmDir/bulma" ${baseSassCmd}
        fi

        if [ "$useBootstrap" = "y" ];
        then
          sass --load-path="$nmDir/bootstrap" ${baseSassCmd}
        fi

        if [ "$useBootstrap" = "n" ] && [ "$useBootstrap" = "n" ];
        then
          sass ${baseSassCmd}
        fi
      fi
    fi
  done
done
