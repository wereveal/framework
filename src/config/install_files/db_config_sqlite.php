<?php
return [
    'driver'   => 'sqlite',
    'host'     => 'REPLACE_ME', // specify name of database file, should be saved in private dir
    'port'     => '3306',       // ignored
    'name'     => 'REPLACE_ME', // not sure if this is used for sqlite
    'user'     => 'REPLACE_ME',
    'password' => 'REPLACE_ME',
    'userro'   => 'REPLACE_ME',
    'passro'   => 'REPLACE_ME',
    'persist'  => false,
    'prefix'   => 'REPLACE_ME'  // table prefix
];
