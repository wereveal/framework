{
  "name": "librarydev",
  "version": "1.0.0",
  "description": "* The Framework upon which I build my apps. * Version 5",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "ritc:/srv/git/ritc/framework"
  },
  "keywords": [],
  "author": "William E Reveal",
  "license": "MIT",
  "dependencies": {
    "bulma": "^0.9.3",
    "bulma-extensions": "^6.2.7",
    "html5shiv": "^3.7.3",
    "leaflet": "^1.7.1",
    "whatwg-fetch": "^3.6.2"
  },
  "devDependencies": {
    "@fortawesome/fontawesome-pro": "^6.1.1",
    "@vizuaalog/bulmajs": "^0.12.1"
  }
}
