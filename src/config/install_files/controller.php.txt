<?php
/**
 * Class {controller_name}Controller.
 * @package {NAMESPACE}_{NSGROUP}
 */
namespace {NAMESPACE}\{NSGROUP}\Controllers;

use Ritc\Library\Interfaces\ControllerInterface;
use Ritc\Library\Services\Di;
use Ritc\Library\Traits\ControllerTraits;
{controller_use}
/**
 * {controller_name} Controller for {NSGROUP}.
 *
 * @author    {author} <{email}>
 * @version   1.0.0-alpha.0
 * @date      {idate}
 * ## Change Log
 * - v1.0.0-alpha.0 - Initial version        - {sdate} {sauthor}
 * @todo {NAMESPACE}/{NSGROUP}/Controllers/{controller_name}Controller.php - Everything
 */
class {controller_name}Controller implements ControllerInterface
{
    use ControllerTraits;
    {controller_vars}
    /**
     * {controller_name}Controller constructor.
     *
     * @param Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->setupController($o_di);
        {controller_construct}
    }
    {controller_method}
}
