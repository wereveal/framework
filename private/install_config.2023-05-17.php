<?php
return [
    'namespace'        => 'Revealing',
    'app_name'         => 'Design',
    'master_app'       => 'true',
    'author'           => 'William E Reveal',
    'short_author'     => 'wer',
    'email'            => 'wer@wereveal.com',
    'db_file'          => 'db_config',
    'db_host'          => 'rd.sqlite',
    'db_type'          => 'sqlite',
    'db_port'          => '3306',
    'db_name'          => 'main',
    'db_user'          => 'bill',
    'db_pass'          => 'let.Bill.14.please',
    'db_local_name'    => '',
    'db_local_host'    => '',
    'db_local_type'    => '',
    'db_local_port'    => '',
    'db_local_user'    => '',
    'db_local_pass'    => '',
    'db_site_name'     => '',
    'db_site_host'     => '',
    'db_site_type'     => '',
    'db_site_port'     => '',
    'db_site_user'     => '',
    'db_site_pass'     => '',
    'db_ro_user'       => '',
    'db_ro_pass'       => '',
    'db_persist'       => 'false',
    'db_errmode'       => 'exception',
    'db_prefix'        => 'rd_',
    'lib_db_prefix'    => 'lib_',
    'superadmin'       => 'let.GSA.14.please',
    'admin'            => 'let.ADM.14.elif',
    'manager'          => 'let.MAN.14.fi',
    'developer_mode'   => 'false',
    'public_path'      => '',
    'base_path'        => '',
    'server_http_host' => '',
	'domain'           => 'revealingdesign',
	'tld'              => 'com',
	'specific_host'    => '',
	'install_host'     => '',
    'main_twig'        => 'true',
    'app_twig_prefix'  => 'rd_',
    'app_theme_name'   => 'base',
    'app_db_prefix'    => 'rd_',
    'a_groups'         => [], // see install_config.commented.txt!
    'a_users'          => []  // see install_config.commented.txt!
];
### Brief ######################################################################
# see install_config.commented.txt for details
# main thing is the db_local_* data is to create a unique db_config_local.php
# file and db_site_* creates a db_config_{specific_host}.php file which can
# be used for different locations (e.g., local workstation, test site, and
# production site (the default))
################################################################################
